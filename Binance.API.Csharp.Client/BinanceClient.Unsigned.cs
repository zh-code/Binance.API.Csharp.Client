﻿using Binance.API.Csharp.Client.Models.Request;
using Binance.API.Csharp.Client.Models.Response;
using Binance.API.Csharp.Client.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Binance.API.Csharp.Client
{
    public partial class BinanceClient
    {
        public async Task<IEnumerable<KlinesResponse>> Klines(KlinesRequest requestObj)
        {
            HttpWebRequest request = null;
            string requestContent = StaticVars.GetQueryString(requestObj);
            string targetUrl = StaticVars.ENDPOINT + requestObj.EndPoint + "?" +
                requestContent;
            targetUrl = targetUrl.Replace("_", "").Replace("&startTime=0", "").Replace("&endTime=0", "");
            request = (HttpWebRequest)WebRequest.Create(targetUrl);
            request.ContentType = "application/json";
            request.Method = requestObj.Method.ToString();

            HttpWebResponse response = null;
            try
            {
                response = (HttpWebResponse)await request.GetResponseAsync();
                using (StreamReader r = new StreamReader(response.GetResponseStream()))
                {
                    string content = await r.ReadToEndAsync();
                    List<List<object>> contentObj = JsonConvert.DeserializeObject<List<List<object>>>(content);
                    List<KlinesResponse> returnObj = new List<KlinesResponse>();
                    foreach (List<object> objList in contentObj)
                    {
                        returnObj.Add(new KlinesResponse
                        {
                            OpenTime = (long)objList[0],
                            OpenPrice = decimal.Parse(objList[1].ToString()),
                            High = decimal.Parse(objList[2].ToString()),
                            Low = decimal.Parse(objList[3].ToString()),
                            Close = decimal.Parse(objList[4].ToString()),
                            Volume = decimal.Parse(objList[5].ToString()),
                            CloseTime = (long)objList[6],
                            Quote = decimal.Parse(objList[7].ToString()),
                            Trades = (long)objList[8],
                            TakerBuyBaseAssetVolume = decimal.Parse(objList[9].ToString()),
                            TakeBuyQuoteAssetVolume = decimal.Parse(objList[10].ToString()),
                            Ignore = decimal.Parse(objList[11].ToString())
                        });
                    }
                    return returnObj;
                }
            }
            catch (WebException ex)
            {
                using (StreamReader r = new StreamReader(((HttpWebResponse)ex.Response).GetResponseStream()))
                {
                    string content = await r.ReadToEndAsync();
                    throw new Exception(content);
                }
            }
        }
    }
}
