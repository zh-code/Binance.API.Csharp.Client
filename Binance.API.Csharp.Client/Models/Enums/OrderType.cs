﻿namespace Binance.API.Csharp.Client.Models.Enums
{
    /// <summary>
    /// Different types of an order.
    /// </summary>
    public enum OrderType
    {
        LIMIT,
        MARKET,
        STOP_LOSS,
        STOP_LOSS_LIMIT,
        TAKE_PROFIT,
        TAKE_PROFIT_LIMIT,
        LIMIT_MAKER
    }
}
