﻿using System.ComponentModel;

namespace Binance.API.Csharp.Client.Models.Enums
{
    public enum TimeInterval
    {
        _1m,
        _3m,
        _5m,
        _15m,
        _30m,
        _1h,
        _2h,
        _4h,
        _6h,
        _12h,
        _1d,
        _3d,
        _1w,
        _1M
    }
}
