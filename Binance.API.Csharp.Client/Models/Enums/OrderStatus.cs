﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Binance.API.Csharp.Client.Models.Enums
{
    public enum OrderStatus
    {
        NEW,
        PARTIALLY_FILLED,
        FILLED,
        CANCELED,
        PENDING_CANCEL,
        REJECTED,
        EXPIRED,
    }
}
