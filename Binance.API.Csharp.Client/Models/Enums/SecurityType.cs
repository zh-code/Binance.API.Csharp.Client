﻿namespace Binance.API.Csharp.Client.Models.Enums
{
    internal enum SecurityType
    {
        NONE,
        TRADE,
        USER_DATA,
        USER_STREAM,
        MARKET_DATA
    }
}
