﻿using Binance.API.Csharp.Client.Models.Enums;
using Binance.API.Csharp.Client.Models.Response;
using Newtonsoft.Json;

namespace Binance.API.Csharp.Client.Models.Request
{
    public class AccountTradeListRequest : IRequest
    {
        [JsonProperty("symbol")]
        public string Symbol { get; set; }
        [JsonProperty("limit")]
        public int Limit { get; set; }
        [JsonProperty("fromId")]
        public long FromId { get; set; }

        [JsonIgnore]
        internal override string EndPoint => "/api/v3/myTrades";
        [JsonIgnore]
        internal override bool IsSigned => true;
        [JsonIgnore]
        internal override ApiMethod Method => ApiMethod.GET;
    }
}
