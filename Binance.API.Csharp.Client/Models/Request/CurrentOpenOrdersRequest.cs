﻿using Binance.API.Csharp.Client.Models.Enums;
using Binance.API.Csharp.Client.Models.Response;
using Newtonsoft.Json;

namespace Binance.API.Csharp.Client.Models.Request
{
    public class CurrentOpenOrdersRequest : IRequest
    {
        [JsonProperty("symbol")]
        public string Symbol { get; set; }

        [JsonIgnore]
        internal new string EndPoint = "/api/v3/openOrders";
        [JsonIgnore]
        internal override bool IsSigned => true;
        [JsonIgnore]
        internal override ApiMethod Method => ApiMethod.GET;

    }
}
