﻿using Binance.API.Csharp.Client.Models.Enums;
using Binance.API.Csharp.Client.Models.Response;
using Newtonsoft.Json;

namespace Binance.API.Csharp.Client.Models.Request
{
    public class AccountInfoRequest : IRequest
    {
        [JsonIgnore]
        internal override string EndPoint => "/api/v3/account";
        [JsonIgnore]
        internal override bool IsSigned => true;
        [JsonIgnore]
        internal override ApiMethod Method => ApiMethod.GET;
    }
}
