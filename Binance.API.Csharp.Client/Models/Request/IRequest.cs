﻿using Binance.API.Csharp.Client.Models.Enums;
using Newtonsoft.Json;

namespace Binance.API.Csharp.Client.Models.Request
{
    public abstract class IRequest
    {
        [JsonProperty("recvWindow")]
        public long RecvWindow = 5000;
        [JsonProperty("timestamp")]
        public virtual long Timestamp { get; set; }

        [JsonIgnore]
        internal virtual bool IsSigned { get; set; }
        [JsonIgnore]
        internal virtual string EndPoint { get; set; }
        [JsonIgnore]
        internal virtual ApiMethod Method { get; set; }
    }

}