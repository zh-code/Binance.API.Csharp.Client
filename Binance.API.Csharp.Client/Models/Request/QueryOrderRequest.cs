﻿using Binance.API.Csharp.Client.Models.Enums;
using Binance.API.Csharp.Client.Models.Response;
using Newtonsoft.Json;

namespace Binance.API.Csharp.Client.Models.Request
{
    public class QueryOrderRequest : IRequest
    {
        [JsonProperty("symbol")]
        public string Symbol { get; set; }
        [JsonProperty("orderId")]
        public long OrderId { get; set; }
        [JsonProperty("origClientOrderId")]
        public string OrigClientOrderId { get; set; }


        [JsonIgnore]
        internal new string EndPoint = "/api/v3/order";
        [JsonIgnore]
        internal override bool IsSigned => true;
        [JsonIgnore]
        internal override ApiMethod Method => ApiMethod.GET;
    }
}
