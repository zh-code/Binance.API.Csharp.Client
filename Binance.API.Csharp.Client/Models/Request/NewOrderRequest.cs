﻿using Binance.API.Csharp.Client.Models.Enums;
using Binance.API.Csharp.Client.Models.Response;
using Newtonsoft.Json;

namespace Binance.API.Csharp.Client.Models.Request
{
    /// <summary>
    /// Create new Order
    /// </summary>
    public class NewOrderRequest : IRequest
    {
        [JsonProperty("symbol")]
        public string Symbol { get; set; }
        [JsonProperty("side")]
        public OrderSide Side { get; set; }
        [JsonProperty("type")]
        public OrderType Type { get; set; }
        [JsonProperty("timeInForce")]
        public TimeInForce TimeInForce { get; set; }
        [JsonProperty("quantity")]
        public decimal Quantity { get; set; }
        [JsonProperty("price")]
        public decimal Price { get; set; }
        [JsonProperty("newClientOrderId")]
        public string NewClientOrderId { get; set; }
        [JsonProperty("stopPrice")]
        public decimal StopPrice { get; set; }
        [JsonProperty("icebergQty")]
        public decimal IcebergQty { get; set; }
        [JsonProperty("newOrderRespType")]
        public string NewOrderRespType = "RESULT";

        [JsonIgnore]
        internal new string EndPoint = "/api/v3/order";
        [JsonIgnore]
        internal override bool IsSigned => true;
        [JsonIgnore]
        internal override ApiMethod Method => ApiMethod.POST;

    }
}
