﻿using Binance.API.Csharp.Client.Models.Enums;
using Binance.API.Csharp.Client.Models.Response;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Binance.API.Csharp.Client.Models.Request
{
    public class AllOrdersRequest : IRequest
    {
        [JsonProperty("symbol")]
        public string Symbol { get; set; }
        [JsonProperty("orderId")]
        public long OrderId { get; set; }
        [JsonProperty("limit")]
        public int Limit { get; set; }

        [JsonIgnore]
        internal new string EndPoint = "/api/v3/allOrders";
        [JsonIgnore]
        internal override bool IsSigned => true;
        [JsonIgnore]
        internal override ApiMethod Method => ApiMethod.GET;
    }
}
