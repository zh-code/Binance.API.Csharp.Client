﻿using Binance.API.Csharp.Client.Models.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Binance.API.Csharp.Client.Models.Request
{
    public class KlinesRequest
    {
        [JsonProperty("symbol")]
        public string Symbol { get; set; }
        [JsonProperty("interval")]
        public TimeInterval Interval { get; set; }
        [JsonProperty("limit")]
        public int Limit => 500;
        [JsonProperty("startTime")]
        public long StartTime { get; set; }
        [JsonProperty("endTime")]
        public long EndTime { get; set; }

        [JsonIgnore]
        internal string EndPoint => "/api/v1/klines";
        [JsonIgnore]
        internal ApiMethod Method => ApiMethod.GET;
    }
}
