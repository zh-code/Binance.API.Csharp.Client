﻿using Binance.API.Csharp.Client.Models.Enums;
using Newtonsoft.Json;

namespace Binance.API.Csharp.Client.Models.Response
{
    public class OrderResponse : IResponse
    {
        [JsonProperty("symbol")]
        public string Symbol { get; set; }
        [JsonProperty("orderId")]
        public long OrderId { get; set; }
        [JsonProperty("clientOrderId")]
        public string ClientOrderId { get; set; }
        [JsonProperty("price")]
        public decimal Price { get; set; }
        [JsonProperty("origQty")]
        public decimal OrigQty { get; set; }
        [JsonProperty("executedQty")]
        public decimal ExecutedQty { get; set; }
        [JsonProperty("status")]
        public OrderStatus Status { get; set; }
        [JsonProperty("timeInForce")]
        public TimeInForce TimeInForce { get; set; }
        [JsonProperty("type")]
        public OrderType Type { get; set; }
        [JsonProperty("side")]
        public OrderSide Side { get; set; }
        [JsonProperty("stopPrice")]
        public decimal StopPrice { get; set; }
        [JsonProperty("icebergQty")]
        public decimal IcebergQty { get; set; }
        [JsonProperty("time")]
        public long Time { get; set; }
        [JsonProperty("isWorking")]
        public bool IsWorking { get; set; }
    }
}
