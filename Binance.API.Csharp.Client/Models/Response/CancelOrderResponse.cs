﻿using Newtonsoft.Json;

namespace Binance.API.Csharp.Client.Models.Response
{
    public class CancelOrderResponse : IResponse
    {
        [JsonProperty("symbol")]
        public string Symbol { get; set; }
        [JsonProperty("origClientOrderId")]
        public string OrigClientOrderId { get; set; }
        [JsonProperty("orderId")]
        public long OrderId { get; set; }
        [JsonProperty("clientOrderId")]
        public string ClientOrderId { get; set; }
    }
}
