﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Binance.API.Csharp.Client.Models.Response
{
    public class KlinesResponse : IResponse
    {
        public long OpenTime { get; set; }
        public decimal OpenPrice { get; set; }
        public decimal High { get; set; }
        public decimal Low { get; set; }
        public decimal Close { get; set; }
        public decimal Volume { get; set; }
        public long CloseTime { get; set; }
        public decimal Quote { get; set; }
        public long Trades { get; set; }
        public decimal TakerBuyBaseAssetVolume { get; set; }
        public decimal TakeBuyQuoteAssetVolume { get; set; }
        public decimal Ignore { get; set; }
    }
}
