﻿using Binance.API.Csharp.Client.Models.Enums;
using Binance.API.Csharp.Client.Models.Request;
using Binance.API.Csharp.Client.Models.Response;
using Binance.API.Csharp.Client.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Binance.API.Csharp.Client
{
    public partial class BinanceClient
    {
        private static string ApiKey { get; set; }
        private static string ApiSecret { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="apiKey"></param>
        /// <param name="apiSecret"></param>
        public BinanceClient(string apiKey, string apiSecret)
        {
            ApiKey = apiKey;
            ApiSecret = apiSecret;
        }

        /// <summary>
        /// Get account info api. All params are set dynamically
        /// </summary>
        /// <returns></returns>
        public async Task<AccountInfoResponse> GetAccountInfo()
        {
            HttpWebRequest request = GetRequest(new AccountInfoRequest());
            HttpWebResponse response = null;
            try
            {
                response = (HttpWebResponse)await request.GetResponseAsync();
                using (StreamReader r = new StreamReader(response.GetResponseStream()))
                {
                    string content = await r.ReadToEndAsync();
                    return JsonConvert.DeserializeObject<AccountInfoResponse>(content);
                }
            }
            catch(WebException ex)
            {
                using (StreamReader r = new StreamReader(((HttpWebResponse)ex.Response).GetResponseStream()))
                {
                    string content = await r.ReadToEndAsync();
                    throw new Exception(content);
                }
            }
            
        }

        /// <summary>
        /// Get trade list
        /// </summary>
        /// <param name="requestObj"><seealso cref="AccountTradeListRequest"/>API Params</param>
        /// <returns></returns>
        public async Task<IEnumerable<AccountTradeListResponse>> GetAccountTradeList(AccountTradeListRequest requestObj)
        {
            HttpWebRequest request = GetRequest(requestObj);
            HttpWebResponse response = null;
            try
            {
                response = (HttpWebResponse)await request.GetResponseAsync();
                using (StreamReader r = new StreamReader(response.GetResponseStream()))
                {
                    string content = await r.ReadToEndAsync();
                    return JsonConvert.DeserializeObject<IEnumerable<AccountTradeListResponse>>(content);
                }
            }
            catch (WebException ex)
            {
                using (StreamReader r = new StreamReader(((HttpWebResponse)ex.Response).GetResponseStream()))
                {
                    string content = await r.ReadToEndAsync();
                    throw new Exception(content);
                }
            }            
        }

        /// <summary>
        /// Get all order API
        /// </summary>
        /// <param name="requestObj"><seealso cref="AllOrdersRequest"/>API params</param>
        /// <returns></returns>
        public async Task<IEnumerable<OrderResponse>> GetAllOrder(AllOrdersRequest requestObj)
        {
            HttpWebRequest request = GetRequest(requestObj);

            HttpWebResponse response = null;
            try
            {
                response = (HttpWebResponse)await request.GetResponseAsync();
                using (StreamReader r = new StreamReader(response.GetResponseStream()))
                {
                    string content = await r.ReadToEndAsync();
                    return JsonConvert.DeserializeObject<IEnumerable<OrderResponse>>(content);
                }
            }
            catch (WebException ex)
            {
                using (StreamReader r = new StreamReader(((HttpWebResponse)ex.Response).GetResponseStream()))
                {
                    string content = await r.ReadToEndAsync();
                    throw new Exception(content);
                }
            }
        }

        /// <summary>
        /// Cancel Order API
        /// </summary>
        /// <param name="requestObj"><seealso cref="CancelOrderRequest"/>API Params</param>
        /// <returns></returns>
        public async Task<IEnumerable<OrderResponse>> CancelOrder(CancelOrderRequest requestObj)
        {
            HttpWebRequest request = GetRequest(requestObj);

            HttpWebResponse response = null;
            try
            {
                response = (HttpWebResponse)await request.GetResponseAsync();
                using (StreamReader r = new StreamReader(response.GetResponseStream()))
                {
                    string content = await r.ReadToEndAsync();
                    return JsonConvert.DeserializeObject<IEnumerable<OrderResponse>>(content);
                }
            }
            catch (WebException ex)
            {
                using (StreamReader r = new StreamReader(((HttpWebResponse)ex.Response).GetResponseStream()))
                {
                    string content = await r.ReadToEndAsync();
                    throw new Exception(content);
                }
            }
        }

        /// <summary>
        /// Get Open Orders API
        /// </summary>
        /// <param name="requestObj"><seealso cref="CurrentOpenOrdersRequest"/>API Params</param>
        /// <returns></returns>
        public async Task<IEnumerable<OrderResponse>> GetOpenOrders(CurrentOpenOrdersRequest requestObj)
        {
            HttpWebRequest request = GetRequest(requestObj);

            HttpWebResponse response = null;
            try
            {
                response = (HttpWebResponse)await request.GetResponseAsync();
                using (StreamReader r = new StreamReader(response.GetResponseStream()))
                {
                    string content = await r.ReadToEndAsync();
                    return JsonConvert.DeserializeObject<IEnumerable<OrderResponse>>(content);
                }
            }
            catch (WebException ex)
            {
                using (StreamReader r = new StreamReader(((HttpWebResponse)ex.Response).GetResponseStream()))
                {
                    string content = await r.ReadToEndAsync();
                    throw new Exception(content);
                }
            }
        }

        /// <summary>
        /// New Order API
        /// </summary>
        /// <param name="requestObj"><seealso cref="NewOrderRequest"/>API Params</param>
        /// <returns></returns>
        public async Task<NewOrderResponse> NewOrder(NewOrderRequest requestObj)
        {
            HttpWebRequest request = GetRequest(requestObj);

            HttpWebResponse response = null;
            try
            {
                response = (HttpWebResponse)await request.GetResponseAsync();
                using (StreamReader r = new StreamReader(response.GetResponseStream()))
                {
                    string content = await r.ReadToEndAsync();
                    return JsonConvert.DeserializeObject<NewOrderResponse>(content);
                }
            }
            catch (WebException ex)
            {
                using (StreamReader r = new StreamReader(((HttpWebResponse)ex.Response).GetResponseStream()))
                {
                    string content = await r.ReadToEndAsync();
                    throw new Exception(content);
                }
            }
        }

        /// <summary>
        /// Query Order Request
        /// </summary>
        /// <param name="requestObj"><seealso cref="QueryOrderRequest"/>API params</param>
        /// <returns></returns>
        public async Task<QueryOrderRequest> QueryOrder(QueryOrderRequest requestObj)
        {
            HttpWebRequest request = GetRequest(requestObj);

            HttpWebResponse response = null;
            try
            {
                response = (HttpWebResponse)await request.GetResponseAsync();
                using (StreamReader r = new StreamReader(response.GetResponseStream()))
                {
                    string content = await r.ReadToEndAsync();
                    return JsonConvert.DeserializeObject<QueryOrderRequest>(content);
                }
            }
            catch (WebException ex)
            {
                using (StreamReader r = new StreamReader(((HttpWebResponse)ex.Response).GetResponseStream()))
                {
                    string content = await r.ReadToEndAsync();
                    throw new Exception(content);
                }
            }
        }

        /// <summary>
        /// Get HttpWebRequest object
        /// </summary>
        /// <param name="requestObj">Request</param>
        /// <returns></returns>
        private HttpWebRequest GetRequest(IRequest requestObj)
        {
            HttpWebRequest request = null;
            requestObj.Timestamp = StaticVars.GetTimeStamp(DateTime.UtcNow);
            string requestContent = StaticVars.GetQueryString(requestObj);
            if (requestObj.IsSigned)
            {
                requestContent = requestContent + "&signature=" + CalcHMACSHA256(requestContent);
            }
            string targetUrl = StaticVars.ENDPOINT + requestObj.EndPoint + "?" +
                requestContent;
            request = (HttpWebRequest)WebRequest.Create(targetUrl);
            request.ContentType = "application/json";
            request.Method = requestObj.Method.ToString();
            request.Headers["X-MBX-APIKEY"] = ApiKey;            

            return request;
        }


        /// <summary>
        /// Calculate HMAC SHA256 for SIGNED api calls 
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private static string CalcHMACSHA256(string message)
        {
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] keyBytes = encoding.GetBytes(ApiSecret);
            byte[] messageBytes = encoding.GetBytes(message);
            HMACSHA256 cryptographer = new HMACSHA256(keyBytes);

            byte[] bytes = cryptographer.ComputeHash(messageBytes);

            return BitConverter.ToString(bytes).Replace("-", "").ToLower();
        }
    }
}
