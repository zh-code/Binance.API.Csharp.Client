﻿using System;
using System.Linq;
using System.Web;

namespace Binance.API.Csharp.Client.Utils
{
    internal static class StaticVars
    {
        internal static string ENDPOINT = "https://api.binance.com";
        
        internal static string GetQueryString(object obj)
        {
            var properties = from p in obj.GetType().GetProperties()
                             where p.GetValue(obj, null) != null
                             select Char.ToLowerInvariant(p.Name[0]) + p.Name.Substring(1) + "=" + HttpUtility.UrlEncode(p.GetValue(obj, null).ToString());

            return String.Join("&", properties.ToArray());
        }

        internal static long GetTimeStamp(DateTime UTC)
        {
            return (long)(UTC.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
        }
    }
}
