using Binance.API.Csharp.Client.Models.Request;
using Binance.API.Csharp.Client.Models.Response;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Binance.API.Csharp.Client.Test
{
    [TestClass]
    public class APITest
    {
        private BinanceClient Client = new BinanceClient("", "");

        [TestMethod]
        public void AccountInfoTest()
        {
            AccountInfoResponse response = Client.GetAccountInfo().Result;

            Assert.AreNotEqual(response, null);
        }

        [TestMethod]
        public void AccountTradeListRequest()
        {
            AccountTradeListRequest request = new AccountTradeListRequest
            {
                Symbol = "LTCBTC"
            };

            IEnumerable<AccountTradeListResponse> response = Client.GetAccountTradeList(request).Result;

            Assert.AreNotEqual(response, null);
        }

        [TestMethod]
        public void AllOrderTest()
        {
            AllOrdersRequest request = new AllOrdersRequest
            {
                Symbol = "LTCBTC"
            };

            IEnumerable<OrderResponse> response = Client.GetAllOrder(request).Result;

            Assert.AreNotEqual(response, null);
        }

        [TestMethod]
        public void KlinesTest()
        {
            KlinesRequest request = new KlinesRequest
            {
                Symbol = "BNBBTC",
                Interval = Models.Enums.TimeInterval._1h,
                StartTime = GetTimeStamp(DateTime.UtcNow.AddDays(-1))
            };

            IEnumerable<KlinesResponse> response = Client.Klines(request).Result;

            Assert.IsNotNull(response);
        }
        internal static long GetTimeStamp(DateTime UTC)
        {
            return (long)(UTC.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
        }

    }
}
